# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Задача №1 (RFQ- Request For Quote) ###

* Извлечение информации из потока электронных писем
* Dataset: https://drive.google.com/drive/folders/1lsg7clIpDlq48Re4zaARPrWfQr1-HC3S

### Задача №2 (PAD - Primary Account Documents) ###

* Извлечение информации из потока бухгалтерских документов
* Dataset: https://drive.google.com/drive/folders/1S5Ij4jGyBtBhsCLgaspHf864CQLuMQ0K

### Задача №3 (PROSPECTUS) ###

* Поиск существенных фактов в многостраничных документах  на примере поиска финансовых ковенант в проспектах эмиссии

* Дано: 100 примеров документов (проспекты эмиссии), для каждого из документов указывается № страницы на которой есть существенное условие (ковенанта) в файле Excel. Ковенанта представляет из себя ключевое слово (фразу) которое может отличаться от случая к случаю.
* Dataset: https://drive.google.com/drive/folders/1XUTA1jrv-fIVQx6685vyOy7aVSsN_Xqq

* Задача: на основании примеров документов обучить модели NLP для классификации каждой страницы документа - есть или нет на странице ковенант (ковенанты) одного из 20 заданных видов (см. примеры). Оценить точность классификации (ошибка I и II рода) 
